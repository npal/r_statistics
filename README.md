Project built for the "Statistical Data Analysis" module of the University of Zagreb's Faculty of Electrical Engineering and Computing. 

Goal of the project was to analyze the Yelp review dataset and draw useful statistical conclusions from it, using the R programming language. The dataset, R programs written, as well as an R Markdown file (in Croatian) containing project conclusions are provided.

Authors: Niko Palić, Sanja Deur, Juraj Vladika